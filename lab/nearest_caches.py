# -*- encoding: utf-8 -*-

import requests
from datetime import datetime
from pprint import pprint

address = raw_input('Podaj adres: ')
#address = u'Nowoursynowska 166, Warszawa'

# Znalezienie współrzędnych adresu
base = 'http://maps.googleapis.com/maps/api/geocode/json'
data = {'address': address, 'sensor': 'false' }
response = requests.get(base, params=data)
lat = response.json()['results'][0]['geometry']['location']['lat']
lng = response.json()['results'][0]['geometry']['location']['lng']

# Znalezienie najbliższych geoskrytek
key='6VHBcmAZPHdRfMQv3tn3'
base2 = 'http://opencaching.pl/okapi/services/caches/search/nearest'
data2 = { 'center': str(lat)+'|'+str(lng), 'consumer_key': key }
response2 = requests.get(base2, params=data2)
caches = '|'.join(response2.json()['results'])

# Wydobycie informacji o lokalizacji skrytek
base = 'http://opencaching.pl/okapi/services/caches/geocaches'
data = { 'cache_codes':caches , 'consumer_key': key }
result = requests.get(base, params=data)
duzyresult = result.json()

# Przydzielenie adresu skrytkom na podstawie ich współrzędnych
skrzynki = []

googlebase = 'http://maps.googleapis.com/maps/api/geocode/json'
for skrytka in duzyresult:
    skryteczka = duzyresult[skrytka]
    lat_lng = skryteczka['location'].split('|')
    googledata = { 'latlng':lat_lng[0]+','+lat_lng[1] , 'sensor': 'false' }
    try:
        result2 = requests.get(googlebase, params=googledata)
        ladnyadres =  (result2.json()['results'][0]['formatted_address'])
        adresskrzynki = [ladnyadres, skryteczka['name'], skryteczka['status']]
        skrzynki.append(adresskrzynki)
        print "proba udana. Program dziala, prosimy nie wylaczac " + str(datetime.now())
    except:
        print "proba nieudana. Program dziala, prosimy nie wylaczac " + str(datetime.now())
        continue

# Wyświetlenie wyniku
skrzynki.sort(key=lambda x: x[2])

for skrzynka in skrzynki:
    print skrzynka[0] + ' | ' + skrzynka[1] + ' | ' + skrzynka[2]
