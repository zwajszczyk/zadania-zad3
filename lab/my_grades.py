# -*- encoding: utf-8 -*-

import getpass
import requests
from bs4 import BeautifulSoup

url = {
    'login': 'https://e.sggw.waw.pl/login/index.php',
    'grades': 'http://e.sggw.waw.pl/grade/report/user/index.php?id=649'
}

username = raw_input('Użytkownik: ')
password = raw_input('Haslo: ')
payload = {
    'username': username,
    'password': password
}

# Logowanie

session = requests.session()
session.post(url['login'], data=payload, verify=False)

# Pobranie strony z ocenami

response = session.get(url['grades'], verify=False)
#print response.status_code
#print response.headers

# Odczyt strony

html = response.text
#print html

# Parsowanie strony
parsed = BeautifulSoup(html)

# Scraping
grades = parsed.find('table', class_='user-grade')
gbody = grades.find('tbody')
tr_all = gbody.find_all('tr')

names_list = []
grades_dict_list = []

for i in range(1,len(tr_all)):
    temp=tr_all[i]
    cells = temp.find_all('td')
    for cell in cells:
        if 'b1t' in cell['class'] and 'b1l' not in cell['class']:
            #print cell.text
            names_list.append(cell.text)
            tempdict=[]
            grades_dict_list.append(tempdict)
    if 'b1b' in cells[0]['class']:
        temp_name = cells[0].find('a').text
        temp_href = cells[0].find('a')['href']
        temp_grade=cells[1].text
        grade_table=[temp_name, temp_href, temp_grade]
        grades_dict_list[-1].append(grade_table)

# Sortowanie

for dict_ in grades_dict_list:
    dict_.sort(key=lambda x: x[2])

# Wyświetlenie posortowanych ocen w kategoriach
bigdict={}
for k in range(0, len(names_list)):
    #print k
    bigdict[names_list[k]] = grades_dict_list[k]
for typ in bigdict:
    print typ
    for grade in bigdict[typ]:
        print grade[0]+' '+grade[1]+' '+grade[2]