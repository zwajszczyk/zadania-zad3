__author__ = 'Zbyszek'
# -*- encoding: utf-8 -*-
# fbcccc26936e4b03f56e533d3afb98f407075c82

import requests
from datetime import datetime
from pprint import pprint
import csv
from bs4 import BeautifulSoup
from pygooglechart import PieChart2D
from pygooglechart import GroupedVerticalBarChart
from pygooglechart import LabelAxis
from pygooglechart import Axis

# funkcja robiaca wykres kolowy
def makePieCharts(data,charts_n,positions_n,width,height,catName,blockfips):
    for k in range(0,charts_n):
        #etykiety
        charttitles = data[k][0][2:]
        # konwertujemy stringi z tabel na dane liczbowe; 2: bo w kolumnie 1 jest 'total', niepotrzebne w wykresie kolowym
        chartdata = [int(i) for i in data[k][1][2:]]
        charttitles2=[];
        chartdata2 = [];
        maxdata1=max(chartdata)
        # wyciaganie w odpowiedniej kolejnosci (wg wielkosci) danych ktore chcemy miec na wykresie
        for h in range(0,positions_n):
            maxdata=max(chartdata)
            maxpos = chartdata.index(maxdata)
            charttitles2.append(charttitles.pop(maxpos))
            chartdata2.append(chartdata.pop(maxpos))
        chart = PieChart2D(width,height)
        # jesli nie ma niezerowych danych, to nie robimy wykresu
        if maxdata1==0:
            continue
        chart.add_data(chartdata2)
        chart.set_pie_labels(charttitles2)
        #print chart.get_url()
        #print chart
        #nadajemy jednoznaczna nazwe, przy okazji zestawienia dla jednego 'badania' beda mialy ten sam prefiks nazwy
        chart.download(str(blockfips[0:12])+catName+data[k][1][0]+str(k)+'p.png')
        # informujemy uzytkownika ze program dziala
        print "wykres kolowy utworzony jako: "+str(blockfips[0:12])+catName+data[k][1][0]+str(k)+'p.png'
        pass

# funkcja robiaca wykres slupkowy
def makeBarCharts(data,charts_n,positions_n,width,height,catName,blockfips):
    for k in range(0,charts_n):
        #etykiety
        charttitles = data[k][0][1:]
        # konwertujemy stringi z tabel na dane liczbowe
        chartdata = [int(i) for i in data[k][1][1:]]
        charttitles2=[];
        chartdata2 = [];
        maxdata1=max(chartdata)
        # wyciaganie w odpowiedniej kolejnosci (wg wielkosci) danych ktore chcemy miec na wykresie
        # w wykresie slupkowym nie bedziemy brac wszystkich danych, tylko najwiekze: dla czytelnosci
        for h in range(0,positions_n):
            maxdata=max(chartdata)
            maxpos = chartdata.index(maxdata)
            charttitles2.append(charttitles.pop(maxpos))
            chartdata2.append(chartdata.pop(maxpos))
        chart = GroupedVerticalBarChart(width,height, y_range=(0,maxdata1+1))
        datalen = len(chartdata2)
        # zabawa w odpowiednia szerokosc slupkow itp
        data_bit_width = int(width/datalen)-1
        chart.set_bar_width(int(data_bit_width*0.5))
        chart.set_bar_spacing(5)
        chart.set_group_spacing(int(data_bit_width*0.4))
        chart.add_data(chartdata2)
        # zabezpieczenie przed psujaca sie lewa osia
        if(maxdata1>=20):
            left_axis = range(0, maxdata1+ 1, int(maxdata1/10))
            left_axis[0] = ''
            chart.set_axis_labels(Axis.LEFT, left_axis)
        chart.set_axis_labels(Axis.BOTTOM, charttitles2)
        #print chart.get_url()
        #print chart
        #nadajemy jednoznaczna nazwe, przy okazji zestawienia dla jednego 'badania' beda mialy ten sam prefiks nazwy
        chart.download(str(blockfips[0:12])+catName+data[k][1][0]+str(k)+'b.png')
        print "wykres slupkowy utworzony jako: "+str(blockfips[0:12])+catName+data[k][1][0]+str(k)+'p.png'
        pass

def makeCsv(data,catName,blockfips):
    datatable3=[]
    temprow=[]
    # przepisywanie naglowkow + dodanie naglowkow z %
    temprow.append(data[0][0][0])
    for i in range (1,len(data[0][0])):
        temprow.append(data[0][0][i])
        temprow.append('% '+data[0][0][i])
    datatable3.append(temprow)
    # przepisywanie danych do formatu liczbowego i odpowiedniego miejsca tabeli + wyliczanie danych procentowych
    for k in range(0,4):
        for j in range(1, len(data[k])):
            temprow =[]
            temprow.append(data[k][j][0])
            for i in range (1,len(data[k][0])):
                if data[k][j][i] !='null':
                    temprow.append(float(data[k][j][i]))
                    if float(temprow[1])!=0:
                        temprow.append((float(temprow[-1])/float(temprow[1]))*100)
                    #zeby sie nie zepsulo
                    else:
                        temprow.append('NA')
                else:
                    temprow.append('NA')
                    temprow.append('NA')
            #robimy z liczb ladne stringi z przecinkami, po polsku
            for i in range(1,len(temprow)):
                if temprow != 'NA':
                    try:
                        temprow[i]=('%.2f' % temprow[i]).replace('.', ',')
                    except:
                        pass
            datatable3.append(temprow)
    # latwo i przyjemnie zapisujemy tabelke do pliku
    with open(str(blockfips[0:12])+catName+'.csv', 'wb') as f:
        writer = csv.writer(f, delimiter=';',quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerows(datatable3)
    print "dane w csv zapisane jako: "+str(blockfips[0:12])+catName+'.csv'

#glowna funkcja, pyta API o dane, obrabia otrzymane dane, wywoluje rysowanie/pisanie do pliku
def processCategory(blockfips, prefix, length, name, toBar, toPie, parsed):
    state_fips = blockfips[0:2] #identyfikator liczbowy stanu, kolejne analogicznie
    county_fips = blockfips[2:5]
    tract_fips = blockfips[5:11]
    blockgroup_fips = blockfips[11]
    datatables=[list]*4 # tak dziala. nie znam dobrze Pythona.
    responses=[str]*4
    data3s=[None]*4
    datatables_s= [] #naprawde inaczej nie chcialo mi dzialac
    datatables_c = []
    datatables_t = []
    datatables_b = []
    # klucz do API
    userkey = 'fbcccc26936e4b03f56e533d3afb98f407075c82'
    base3 = 'http://api.census.gov/data/2011/acs5'
    getparams3 ='NAME,'
    # ilosc danych z danej kategorii ktore chcemy pobrac
    cat_len = length
    # API przyjmuje max 50 zmiennych, wzialem z zapasem 40
    req_vars = 40
    # wlasciwe zapytanie, z danymi zwroconymi w dosc nieprzyjazny sposob
    for i in range(1,cat_len):
        getparams3+=prefix+"_%03dE" % (i)
        if i%req_vars==0 or i==cat_len-1:
            data3s[0] = {'key': userkey, 'get': getparams3, 'for': 'state:'+state_fips}
            data3s[1] = {'key': userkey, 'get': getparams3, 'for': 'county:'+county_fips, 'in': 'state:'+state_fips}
            data3s[2] = {'key': userkey, 'get': getparams3, 'for': 'tract:'+tract_fips, 'in': 'state:'+state_fips+'+county:'+county_fips}
            data3s[3] = {'key': userkey, 'get': getparams3, 'for': 'block group:'+blockgroup_fips, 'in': 'state:'+state_fips+'+county:'+county_fips+'+tract:'+tract_fips}
            for j in range(0,4):
                responses[j]=requests.get(base3, params=data3s[j])
            datatables_s.append(responses[0].json()) #tak, to jest straszne. ale inaczej nie umiem.
            datatables_c.append(responses[1].json())
            datatables_t.append(responses[2].json())
            datatables_b.append(responses[3].json())
            getparams3=''
        else:
            getparams3+=','
    datatables[0]=datatables_s
    datatables[1]=datatables_c
    datatables[2]=datatables_t
    datatables[3]=datatables_b #koniec strasznej czesci
    datatables2 = []
    # przepisywanie brzydkich tablic w ladne tablice
    for k in range(0,4):
        datatable2temp=datatables[k][0]
        for i in range(0,len(datatable2temp)):
            for j in range(1,len(datatables[k])):
                datatable2temp[i]+=datatables[k][j][i]
        datatables2.append(datatable2temp)
    #wyrzucanie smieciowych dla nas pol informujacych nas o FIPS stanu itd.
    for k in range(0,4):
        for i in xrange(0,len(datatables2[k][0])):
            if datatables2[k][0][i] in ['state', 'county', 'tract', 'block group']:
                while datatables2[k][0][i] in ['state', 'county', 'tract', 'block group']:
                    #print datatables2[k][0][i]
                    for j in xrange(0,len(datatables2[k])):
                        del datatables2[k][j][i]
                        #print 'usunalem'+str(k)+'.'+str(j)+'.'+str(i)
                    if i+1>len(datatables2[k][0]):
                        break
            if i+1>=len(datatables2[k][0]):
                break
        #pprint(datatables2[k])
        #pprint(len(datatables2[k][0]))
    # zamiana id zmiennej na ladna nazwe
    for i in xrange(0,len(datatables2[0][0])):
        if datatables2[0][0][i]!='NAME':
            tempfind = parsed.find('p', id=datatables2[0][0][i])
            for k in range(0,4):
                datatables2[k][0][i]=tempfind.text
    # jesli jakies dane a przeznaczone do wykresu, to robimy wykresy
    if toBar>0:
        makeBarCharts(datatables2,3,toBar,800,350,name,blockfips)
    if toPie>0:
        makePieCharts(datatables2,3,toPie,700,400,name,blockfips)

    makeCsv(datatables2,name,blockfips)


address = raw_input('Podaj adres w USA, akceptowalny przez GoogleMaps: ')
#przykladowe dzialajace adresy:
#address = '77 Massachusetts Avenue, Cambridge, MA' #MIT
#address = '14721 S Gibson Ave, Compton CA' #chyba nic waznego
#address='1600 Pennsylvania Avenue NW, Washington, DC' #Bialy Dom
#address='135 Benefit Street, Providence, Rhode Island' #cos z ktoregos opowiadania Lovecrafta
#address='610 Waring Ave Bronx, NY'#costam w bronxie
#address='2101 Waverley Street, Palo Alto, CA' #dom Jobsa

#przyporzadkowanie kody->nazwy zmiennych i kategorii. oryginal byl w xml i mial 8 MB, tu jest fragment
legend = open('legend.html').read()
parsed=BeautifulSoup(legend)

#pytamy o wspolrzedne
base = 'http://maps.googleapis.com/maps/api/geocode/json'
data = {'address': address, 'sensor': 'false' }
response = requests.get(base, params=data)
try:
    lat = response.json()['results'][0]['geometry']['location']['lat']
    lng = response.json()['results'][0]['geometry']['location']['lng']
    formatted_address = response.json()['results'][0]['formatted_address']
except:
    print "Temu adresowi nie odpowiada zadna wspolrzedna geograficzna"
    exit(3)

print 'Szerokosc: '+str(lat) + ' Dlugosc: ' + str(lng)
print 'Adres: ' + formatted_address

# pytamy o dane administracyjne dla naszych wspolrzednych
base2 = 'http://data.fcc.gov/api/block/find'
data2 = {'latitude': lat, 'longitude': lng, 'format': 'json', 'showall': True }
response2 = requests.get(base2, params=data2)
#print response2.json()
try:
    block_fips = response2.json()['Block']['FIPS']
    state_name = response2.json()['State']['name']
    state_code = response2.json()['State']['code']
    county_name = response2.json()['County']['name']
    state_fips = block_fips[0:2]
    county_fips = block_fips[2:5]
    tract_fips = block_fips[5:11]
    blockgroup_fips = block_fips[11]
except:
    print 'Adres najwyrazniej nie pasuje do zadnego miejsca w USA'
    exit(4)

print 'Badany stan: ' + state_name + ' (' + state_code +'), hrabstwo: ' + county_name +', nr FIPS bloku: ' + block_fips

processCategory(block_fips,'B04006',110,'ReportedAncestry',8,0,parsed)
processCategory(block_fips,'B02001',9,'Race',0,7,parsed)
processCategory(block_fips,'B03003',4,'Latino',0,2,parsed)
processCategory(block_fips,'B01001',50,'SexByAge',20,0,parsed)
processCategory(block_fips,'B19101',18,'YearlyFamilyIncome',0,16,parsed)
processCategory(block_fips,'C24050',15,'IndustryOccupation',0,13,parsed)
print 'Dzialanie zakonczone sukcesem, wyniki w katalogu z programem'